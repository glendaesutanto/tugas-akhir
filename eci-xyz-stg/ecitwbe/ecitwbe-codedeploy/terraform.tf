provider "aws" {
  region = "ap-southeast-1"
}

terraform {
  backend "s3" {
    bucket         = "default-terraform-state-ap-southeast-1-{account_id}"
    dynamodb_table = "default-terraform-state-ap-southeast-1-{account_id}"
    key            = "/path/to/ecitwbe-codedeploy/tfstate"
    region         = "ap-southeast-1"
    encrypt        = true
  }
}
