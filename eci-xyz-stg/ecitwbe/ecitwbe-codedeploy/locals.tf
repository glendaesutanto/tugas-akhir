locals {
  service_name          = "ecitwbe"
  team_name             = "ecitbwp"
  product_domain        = "eci"
  environment           = "staging"
  deployment_group_name = "ecitwbe-main"
}
