data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "terraform_remote_state" "target_environment" {
  backend = "s3"

  config = {
    key    = "/path/to/target-environment/tfstate"
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
    region = data.aws_region.current.name
  }
}

data "terraform_remote_state" "ecs_cluster" {
  backend = "s3"

  config = {
    key    = "/path/to/eci-ecs-cluster/tfstate"
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
    region = data.aws_region.current.name
  }
}

data "terraform_remote_state" "fargate_service" {
  backend = "s3"

  config = {
    key    = "path/to/ecitwbe-ecs-fargate/tfstate"
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
    region = data.aws_region.current.name
  }
}
