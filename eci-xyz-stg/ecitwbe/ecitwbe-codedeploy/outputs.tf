output "application_name" {
  value       = aws_codedeploy_deployment_group.main.app_name
  description = "Name of codedeploy"
}

output "deployment_group_name" {
  value       = aws_codedeploy_deployment_group.main.deployment_group_name
  description = "Name of codedeploy deployment group"
}
