resource "aws_codedeploy_app" "ecitwbe" {
  compute_platform = "ECS"
  name             = local.service_name
}

resource "aws_codedeploy_deployment_group" "main" {
  app_name              = aws_codedeploy_app.ecitwbe.name
  deployment_group_name = local.deployment_group_name

  deployment_config_name = "CodeDeployDefault.ECSCanary10Percent5Minutes"
  service_role_arn       = data.terraform_remote_state.target_environment.outputs.codedeploy_role_arn

  ecs_service {
    cluster_name = data.terraform_remote_state.ecs_cluster.outputs.name
    service_name = data.terraform_remote_state.fargate_service.outputs.ecs_service_name
  }

  load_balancer_info {
    target_group_pair_info {
      prod_traffic_route {
        listener_arns = [
          data.terraform_remote_state.fargate_service.outputs.listener_arn
        ]
      }

      target_group {
        name = data.terraform_remote_state.fargate_service.outputs.tg_active_name
      }

      target_group {
        name = data.terraform_remote_state.fargate_service.outputs.tg_standby_name
      }
    }
  }

  auto_rollback_configuration {
    enabled = true
    events  = ["DEPLOYMENT_FAILURE"]
  }

  blue_green_deployment_config {
    deployment_ready_option {
      # if we want to route traffic to the new cluster manually
      # action_on_timeout = "STOP_DEPLOYMENT"
      # wait_time_in_minutes = 7

      action_on_timeout = "CONTINUE_DEPLOYMENT"
    }

    terminate_blue_instances_on_deployment_success {
      action = "TERMINATE"
      # a deployment will be finished after the blue (old) instances are terminated
      termination_wait_time_in_minutes = 5
    }
  }

  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "BLUE_GREEN"
  }

  trigger_configuration {
    trigger_events = [
      "DeploymentSuccess",
      "DeploymentFailure",
      "DeploymentRollback"
    ]
    trigger_name       = "deployment-notifications"
    trigger_target_arn = data.terraform_remote_state.target_environment.outputs.deployment_notification_topic_arn
  }
}
