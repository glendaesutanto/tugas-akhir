[
  {
    "name": "app",
    "image": "{ecr_repository_url}/${service_name}-app:${service_version}",
    "essential": true,
    "environment": [
        {
          "name": "JAVA_TOOL_OPTIONS",
          "value": "-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.port=8090 -Dcom.sun.management.jmxremote.rmi.port=8090"
        },
        {
          "name": "TRAVELOKA_ENV",
          "value": "stg"
        },
        {
          "name": "environment",
          "value": "staging"
        },
        {
          "name": "LOG4J_FORMAT_MSG_NO_LOOKUPS",
          "value": "true"
        }
    ],
    "dockerLabels": {
      "SERVICE_NAME": "${service_name}",
      "PRODUCT_DOMAIN": "${product_domain}",
      "CLUSTER": "${cluster}",
      "com.datadoghq.ad.instances": "[{\"host\": \"%%host%%\", \"port\": 8090, \"conf\": [{\"include\": {\"attribute\": {\"Usage.max\": {\"metric_type\": \"gauge\"},\"Usage.used\": {\"metric_type\": \"gauge\"}},\"domain\": \"java.lang\",\"type\": \"MemoryPool\"}},{\"include\": {\"attribute\": {\"CollectionCount\": {\"metric_type\": \"gauge\"}},\"domain\": \"java.lang\",\"type\": \"GarbageCollector\"}}]}]",
      "com.datadoghq.ad.check_names": "[\"jmx\"]",
      "com.datadoghq.ad.init_configs": "[{\"is_jmx\": true, \"collect_default_metrics\": true}]"
    },
    "secrets": [

    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${log_name}",
        "awslogs-region": "${aws_region}",
        "awslogs-stream-prefix": "app-${service_version}-"
      }
    },
    "portMappings": [
      {
        "protocol": "tcp",
        "containerPort": 8080
      }
    ],
    "dependsOn": [
        {
            "containerName": "datadog",
            "condition": "START"
        }
    ]
  },
  ...
]
