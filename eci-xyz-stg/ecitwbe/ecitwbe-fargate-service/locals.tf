locals {
  service_name   = "ecitwbe"
  team_name      = "ecitbwp"
  product_domain = "eci"
  environment    = "staging"
  app_type       = "java"
  cluster_role   = "app"
  cluster        = "${local.service_name}-${local.cluster_role}"
  description    = "Fargate service for ECI TBWP Backend ECITWBE Service"

  service_port              = 8080
  container_defenition_path = data.template_file.container_definitions.rendered
  cpu                       = 256
  memory                    = 1024
  platform_version          = "1.4.0"

  lb_internal = "true"
  zone_id     = data.terraform_remote_state.private_zone.outputs.private_zone_id
  cert_arn    = data.terraform_remote_state.private_certificate.outputs.acm_certificate_arn

  lb_allowed_cidr = ["list-of-allowed-cidrs"]

  db_sg_id                = data.terraform_remote_state.db.outputs.postgres_security_group_id
  min_capacity            = 1
  max_capacity            = 1
  has_db                  = "true"
  db_port                 = "5432"
  ecr_name                = "{ecr_repository_url}/${local.service_name}-app"
  autoscaling_target_rpm  = 6000
  autoscaling_target_cpu  = 70
  tg_health_check_period  = 400
  lb_idle_timeout         = 30
  tg_deregistration_delay = 30
  tg_target_type          = "ip"
  tg_interval             = 30
  tg_healthy_threshold    = 3
  tg_unhealthy_threshold  = 3
  tg_timeout              = 5
  tg_healthcheck_path     = "/healthcheck"
  vpc_id                  = data.terraform_remote_state.vpc.outputs.vpc_id
  subnet_app_ids          = data.terraform_remote_state.vpc.outputs.subnet_app_ids
  subnet_public_ids       = data.terraform_remote_state.vpc.outputs.subnet_public_ids
  alb_log_bucket          = "default-elb-logs-ap-southeast-1-{account_id}"
  service_version         = "latest"
  log_retention_in_days   = 14

  global_tags = {
    Service       = "${local.service_name}"
    Cluster       = "${local.cluster}"
    Application   = "${local.app_type}"
    ProductDomain = "${local.product_domain}"
    Environment   = "${local.environment}"
    ManagedBy     = "terraform"
    Team          = "${local.team_name}"
  }

  log_tags = {}
}
