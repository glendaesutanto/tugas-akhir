resource "aws_security_group_rule" "ingress_ecitwfe_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "Ingress from ecitwfe to ecitwbe"
}

resource "aws_security_group_rule" "ingress_xpeops-app-multi-account_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "inbound from xpeops-app multi-account"
}

resource "aws_security_group_rule" "ingress_xpetool-app-multi-account_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "inbound from xpetool-app multi-account"
}

resource "aws_security_group_rule" "ingress_tbi-app-sg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "Ingress from tbi app sg to ecitwbe"
}

resource "aws_security_group_rule" "ingress_xpebook-app-multi-account_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "Ingress from xpebook-app multi-account"
}

resource "aws_security_group_rule" "ingress_ecitbic-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from ecitbic-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_payment-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from payment-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_payment-app_multi-account_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from payment-app multi-account to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_payctx-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from payctx-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_payptc-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from payptc-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_conissu-app-tvlk-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from conissu-app tvlk-stg to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_conissu-app-tvlk-con-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from conissu-app to ecitwbe-lbint"
}


resource "aws_security_group_rule" "ingress_contool-app-tvlk-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from contool-app tvlk-stg to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_contool-app-tvlk-con-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from contool-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_ebiissu-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from ebiissu-app (tvlk-ebi-prod) to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_culops-app-tvlk-cul-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from culops-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_cultool-app-tvlk-cul-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from cultool-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_xxttx-app-tvlk-xxt-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from xxttx-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_xxtdata-app-tvlk-xxt-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from xxtdata-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_xxtsrch-app-tvlk-xxt-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from xxtsrch-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_cnmops-app-tvlk-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from cnmops-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_tvlk-cnm-stg_cnmops-app-tvlk-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from tvlk-cnm-stg cnmops-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_gvoops-app-gvofrp-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from gvoops-app and gvofrp-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_insissu-app-tvlk-ins-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from insissu-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_insclm-app-tvlk-ins-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from insclm-app to ecitwbe-lbint"
}

# insissu is still in progress migrating to multi account, so we add both ingress
resource "aws_security_group_rule" "ingress_insissu-app-tvlk-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from insissu-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_ipigrfd-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from ipigrfd-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_ipiprf-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from ipiprf-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_eciaebi-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from eciaebi-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_ecixpbi-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from ecixpbi-app to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_trnops-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from trnops-app in tvlk-stg to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_gtrtnop-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from gtrtnop-app in tvlk-gtr-stg to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_gtratop-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from gtratop-app in tvlk-gtr-stg to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_gtrbuop-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from gtrbuop-app tvlk-gtr-stg to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_pprops-app_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from pprops-app tvlk-ppr-stg to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_trppops-app-tvlk-trp-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from trppops-app tvlk-trp-stg to ecitwbe-lbint"
}

resource "aws_security_group_rule" "ingress_payptc-app-tvlk-pay-stg_to_ecitwbe_443" {
  type                     = "ingress"
  security_group_id        = "${module.fargate_service.lb_sg_id}"
  source_security_group_id = "{team_account_id}/sg-xxxxxxx"
  from_port                = "443"
  to_port                  = "443"
  protocol                 = "tcp"
  description              = "ingress from payptc-app tvlk-pay-stg to ecitwbe-lbint"
}
