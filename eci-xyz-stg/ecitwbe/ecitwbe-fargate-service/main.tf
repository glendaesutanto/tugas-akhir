module "fargate_service" {
  source                    = "github.com/xyz/terraform-aws-eci-fargate-service/tree/v3.0.4/modules/service-with-alb-codedeploy"
  environment               = local.environment
  lb_allowed_cidr           = local.lb_allowed_cidr
  lb_internal               = local.lb_internal
  container_defenition_path = local.container_defenition_path
  cpu                       = local.cpu
  memory                    = local.memory
  app_type                  = local.app_type
  platform_version          = local.platform_version

  min_capacity = local.min_capacity
  max_capacity = local.max_capacity

  zone_id  = local.zone_id
  db_sg_id = local.db_sg_id
  cert_arn = local.cert_arn
  has_db   = local.has_db
  ecr_name = local.ecr_name
  db_port  = local.db_port

  autoscaling_target_rpm  = local.autoscaling_target_rpm
  autoscaling_target_cpu  = local.autoscaling_target_cpu
  tg_health_check_period  = local.tg_health_check_period
  service_name            = local.service_name
  product_domain          = local.product_domain
  service_port            = local.service_port
  lb_idle_timeout         = local.lb_idle_timeout
  tg_deregistration_delay = local.tg_deregistration_delay
  tg_target_type          = local.tg_target_type
  tg_interval             = local.tg_interval
  tg_healthy_threshold    = local.tg_healthy_threshold
  tg_unhealthy_threshold  = local.tg_unhealthy_threshold
  tg_timeout              = local.tg_timeout
  tg_healthcheck_path     = local.tg_healthcheck_path
  lb_logs_s3_bucket_name  = local.alb_log_bucket

  vpc_id            = local.vpc_id
  subnet_app_ids    = local.subnet_app_ids
  subnet_public_ids = local.subnet_public_ids
  task_role_arn     = module.task_role.role_arn

  tags = {
    Service       = local.service_name
    Description   = local.description
    Cluster       = local.cluster
    Application   = local.app_type
    ProductDomain = local.product_domain
    Environment   = local.environment
    ManagedBy     = "terraform"
    Team          = local.team_name
  }

  lb_tags = {
    Team = local.team_name
  }

  tg_tags = {
    Team = local.team_name
  }
}

module "task_role" {
  source = "github.com/traveloka/terraform-aws-iam-role/tree/v2.0.2/modules/service"

  role_identifier            = "${local.service_name}-task"
  role_description           = "Task Role for ECS Task ${local.service_name}"
  role_force_detach_policies = false

  aws_service    = "ecs-tasks.amazonaws.com"
  environment    = local.environment
  product_domain = local.product_domain
}

resource "aws_iam_role_policy" "task" {
  role   = module.task_role.role_name
  policy = data.aws_iam_policy_document.task.json
}

module "log_group_name" {
  source = "github.com/traveloka/terraform-aws-resource-naming/tree/v0.17.1"

  name_prefix   = "/tvlk/app-java/${local.service_name}"
  resource_type = "cloudwatch_log_group"
}

resource "aws_cloudwatch_log_group" "log_group" {
  name              = module.log_group_name.name
  retention_in_days = local.log_retention_in_days

  tags = merge(local.global_tags, local.log_tags)
}
