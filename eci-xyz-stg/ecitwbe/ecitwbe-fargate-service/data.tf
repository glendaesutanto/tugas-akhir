data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "terraform_remote_state" "private_zone" {
  backend = "s3"

  config = {
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
    key    = "/path/to/private-zone/tfstate"
    region = data.aws_region.current.name
  }
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
    key    = "/path/to/eci/stg/vpc/tfstate"
    region = data.aws_region.current.name
  }
}

data "terraform_remote_state" "private_certificate" {
  backend = "s3"

  config = {
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
    key    = "/path/to/private-certificate/tfstate"
    region = data.aws_region.current.name
  }
}

data "terraform_remote_state" "tvlk_kms_sqs" {
  backend = "s3"

  config = {
    key    = "/path/to/tvlk_kms_sqs/tfstate"
    region = data.aws_region.current.name
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
  }
}

data "aws_iam_policy_document" "task" {
  statement {
    effect = "Allow"

    actions = [
      "kms:Decrypt",
    ]

    resources = [
      data.terraform_remote_state.tvlk_kms_sqs.outputs.key_arn,
    ]
  }

  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:DescribeLogStreams",
      "logs:PutLogEvents",
      "logs:PutRetentionPolicy",
    ]

    resources = [
      "*",
    ]
  }
}

data "template_file" "container_definitions" {
  template = file("container-definition.tpl")

  vars = {
    aws_region      = data.aws_region.current.name
    service_name    = local.service_name
    product_domain  = local.product_domain
    cluster         = local.cluster
    log_name        = module.log_group_name.name
    service_version = "latest"
  }
}

data "terraform_remote_state" "db" {
  backend = "s3"

  config = {
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
    key    = "/path/to/ecitwbe/db/tfstate"
    region = "${data.aws_region.current.name}"
  }
}
