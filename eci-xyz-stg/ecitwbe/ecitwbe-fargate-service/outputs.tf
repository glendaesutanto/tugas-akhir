output "ecs_service_name" {
  value       = module.fargate_service.ecs_service_name
  description = "Name of the fargate service"
}

output "app_sg_id" {
  value       = module.fargate_service.app_sg_id
  description = "Security group id for the app target group"
}

output "lb_sg_id" {
  value       = module.fargate_service.lb_sg_id
  description = "Security group id for the load balancer"
}

output "aws_route53_record" {
  value       = module.fargate_service.aws_route53_record
  description = "Endpoint to access the application"
}

output "task_role_arn" {
  value       = module.task_role.role_arn
  description = "ARN of IAM role to be used by the task"
}

output "alb_main_arn" {
  value       = module.fargate_service.alb_main_arn
  description = "arn for fargate service alb"
}

output "tg_active_name" {
  value       = module.fargate_service.tg_active_name
  description = "The name of the active target group"
}

output "tg_standby_name" {
  value       = module.fargate_service.tg_standby_name
  description = "The name of the standby target group"
}

output "listener_arn" {
  value       = module.fargate_service.listener_arn
  description = "The ARN of the listener"
}
