resource "aws_codepipeline" "deployment_pipeline" {
  name     = local.deployment_name
  role_arn = data.terraform_remote_state.pipeline_environment.outputs.codepipeline_role_arn

  artifact_store {
    location = data.terraform_remote_state.pipeline_environment.outputs.codepipeline_artifact_bucket_name
    type     = "S3"

    encryption_key {
      id   = data.terraform_remote_state.pipeline_environment.outputs.codepipeline_artifact_encryption_key_id
      type = "KMS"
    }
  }

  stage {
    name = "Sources"

    action {
      name             = "GetBuildManifest"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version          = "1"
      output_artifacts = ["BuildManifest"]

      configuration = {
        S3Bucket    = data.terraform_remote_state.appbin.outputs.artifactory_s3_bucket_name
        S3ObjectKey = local.build_manifest_path
      }
    }
  }

  stage {
    name = "Staging"
    action {
      name     = "EngineerApproval"
      category = "Approval"
      owner    = "AWS"
      provider = "Manual"
      version  = "1"

      configuration = {
        NotificationArn = data.terraform_remote_state.pipeline_environment.outputs.codepipeline_approval_notification_topic_arn
        CustomData = jsonencode({
          target                = "ECS"
          buildManifestLocation = "Sources:GetBuildManifest:BuildManifest"
          pipelineExecutionId   = "#{codepipeline.PipelineExecutionId}"
          notificationTargetKey = "engineers_bp"
          notificationContexts = [
            "ServiceName: ecitwbe"
          ]
        })
      }
      run_order = "1"
    }

    action {
      name             = "PrepareCodeDeploy"
      category         = "Invoke"
      owner            = "AWS"
      provider         = "Lambda"
      input_artifacts  = ["BuildManifest"]
      output_artifacts = ["StagingCodeDeployArtifact"]
      version          = "1"

      configuration = {
        FunctionName = data.terraform_remote_state.pipeline_environment.outputs.ecs_predeployment_function_name
        UserParameters = jsonencode({
          # must be the same as the appspec name prefix, e.g. productionAppspec --> production, and the deployment_jump_role_arns_map's key
          environment                 = "staging"
          codedeploy_application      = data.terraform_remote_state.codedeploy.outputs.application_name
          codedeploy_deployment_group = data.terraform_remote_state.codedeploy.outputs.deployment_group_name
        })
      }

      run_order = "2"
    }

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "CodeDeployToECS"
      input_artifacts = ["StagingCodeDeployArtifact"]
      version         = "1"

      configuration = {
        ApplicationName                = data.terraform_remote_state.codedeploy.outputs.application_name
        DeploymentGroupName            = data.terraform_remote_state.codedeploy.outputs.deployment_group_name
        TaskDefinitionTemplateArtifact = "StagingCodeDeployArtifact"
        AppSpecTemplateArtifact        = "StagingCodeDeployArtifact"
        Image1ArtifactName             = "StagingCodeDeployArtifact"
        Image1ContainerName            = "IMAGE_NAME"
        # coupled with the predeployment function
        TaskDefinitionTemplatePath = "taskdef.json"
        AppSpecTemplatePath        = "appspec.json"
      }
      role_arn  = data.terraform_remote_state.target_environment.outputs.ecs_deployment_jump_role_arn
      run_order = "3"
    }
  }

  stage {
    name = "Production"
    action {
      name     = "EngineerApproval"
      category = "Approval"
      owner    = "AWS"
      provider = "Manual"
      version  = "1"

      configuration = {
        NotificationArn = data.terraform_remote_state.pipeline_environment.outputs.codepipeline_approval_notification_topic_arn
        CustomData = jsonencode({
          target                = "ECS"
          buildManifestLocation = "Sources:GetBuildManifest:BuildManifest"
          pipelineExecutionId   = "#{codepipeline.PipelineExecutionId}"
          notificationTargetKey = "engineers_bp_prod"
          notificationContexts = [
            "ServiceName: ecitwbe"
          ]
        })
      }
      run_order = "4"
    }

    action {
      name             = "PrepareCodeDeploy"
      category         = "Invoke"
      owner            = "AWS"
      provider         = "Lambda"
      input_artifacts  = ["BuildManifest"]
      output_artifacts = ["ProductionCodeDeployArtifact"]
      version          = "1"

      configuration = {
        FunctionName = data.terraform_remote_state.pipeline_environment.outputs.ecs_predeployment_function_name
        UserParameters = jsonencode({
          # must be the same as the appspec name prefix, e.g. productionAppspec -> production, and the deployment_jump_role_arns_map's key
          environment                 = "production"
          codedeploy_application      = "ecitwbe"      # hardcode for ecitwbe-codedeploy in eci-prod
          codedeploy_deployment_group = "ecitwbe-main" # hardcode for ecitwbe-codedeploy in eci-prod
        })
      }

      run_order = "5"
    }

    action {
      name            = "Deploy"
      category        = "Deploy"
      owner           = "AWS"
      provider        = "CodeDeployToECS"
      input_artifacts = ["ProductionCodeDeployArtifact"]
      version         = "1"

      configuration = {
        ApplicationName                = "ecitwbe"      # hardcode for ecitwbe-codedeploy in eci-prod
        DeploymentGroupName            = "ecitwbe-main" # hardcode for ecitwbe-codedeploy in eci-prod
        TaskDefinitionTemplateArtifact = "ProductionCodeDeployArtifact"
        AppSpecTemplateArtifact        = "ProductionCodeDeployArtifact"
        Image1ArtifactName             = "ProductionCodeDeployArtifact"
        Image1ContainerName            = "IMAGE_NAME"
        # coupled with the predeployment function
        TaskDefinitionTemplatePath = "taskdef.json"
        AppSpecTemplatePath        = "appspec.json"
      }
      role_arn  = "arn:aws:iam::{account_id}:role/crossaccount/be_dss-ecs_deployment_jump_role" # hardocode for value in eci-prod target env
      run_order = "6"
    }
  }

  tags = {
    Service                     = local.service_name
    ProductDomain               = local.product_domain
    ManagedBy                   = "terraform"
    Team                        = local.team_name
    OrgempBuildManifestLocation = "Sources:GetBuildManifest:BuildManifest"
    OrgempFailingActions        = "Production:Deploy"
  }
}
