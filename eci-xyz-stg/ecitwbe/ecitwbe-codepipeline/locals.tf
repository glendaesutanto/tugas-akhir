locals {
  service_name        = "ecitwbe"
  team_name           = "ecitbwp"
  product_domain      = "eci"
  environment         = "staging"
  deployment_name     = "ecitwbe-deployment"
  build_manifest_path = "ecitwbe/build_manifest.json"
}
