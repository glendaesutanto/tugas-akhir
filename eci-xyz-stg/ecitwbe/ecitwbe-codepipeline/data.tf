data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

data "terraform_remote_state" "appbin" {
  backend = "s3"

  config = {
    key    = "/path/to/eci-appbin/tfstate"
    region = data.aws_region.current.name
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
  }
}

data "terraform_remote_state" "pipeline_environment" {
  backend = "s3"

  config = {
    key    = "/path/to/pipeline-environment/tfstate"
    region = data.aws_region.current.name
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
  }
}

data "terraform_remote_state" "target_environment" {
  backend = "s3"

  config = {
    key    = "/path/to/target-environment/tfstate"
    region = data.aws_region.current.name
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
  }
}

data "terraform_remote_state" "codedeploy" {
  backend = "s3"

  config = {
    key    = "/path/to/ecitwbe-codedeploy/tfstate"
    region = data.aws_region.current.name
    bucket = "default-terraform-state-${data.aws_region.current.name}-${data.aws_caller_identity.current.account_id}"
  }
}
