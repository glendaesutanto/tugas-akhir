terraform {
  backend "s3" {
    bucket         = "default-terraform-state-ap-southeast-1-{account_id}"
    dynamodb_table = "default-terraform-state-ap-southeast-1-{account_id}"
    key            = "/path/to/ecitwbe-codebuild/tfstate"
    region         = "ap-southeast-1"
    encrypt        = true
  }
}
