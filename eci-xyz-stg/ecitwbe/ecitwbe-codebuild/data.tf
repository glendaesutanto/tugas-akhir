data "template_file" "build_buildspec" {
  template = <<EOF
version: 0.2
phases:
  install:
    runtime-versions:
      java: corretto8
  build:
    commands:
      - chmod +x ./scripts/*
      - ./scripts/assume_role.sh -r arn:aws:iam::{account_id}:role/{role_name}_${local.product_domain}
      - ./scripts/build_eci.sh
  post_build:
    commands:
      - chmod +x ./scripts/*
      - ./scripts/post_build_eci.sh
EOF
}
