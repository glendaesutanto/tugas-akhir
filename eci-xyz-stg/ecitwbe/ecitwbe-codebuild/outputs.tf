output "build_project_name" {
  value       = module.this.build_project_name
  description = "The name of the build codebuild project"
}

output "build_source" {
  value       = module.this.build_source
  description = "The build source"
}
