module "this" {
  source          = "github.com/xyz/terraform-aws-codebuild/tree/v0.13.3"
  build_type      = local.build_type
  image           = local.image
  name            = local.codebuild_name
  product_domain  = local.product_domain
  buildspec       = data.template_file.build_buildspec.rendered
  description     = "${local.repository_name} build, test, and publishing projects"
  additional_tags = local.additional_tags
  environment     = local.environment

  codebuild_role_arn    = local.codebuild_role_arn
  source_repository_url = "https://path.to/${local.repository_name}.git"
  webhook_branch_filter = "ecitwbe/[a-z]+/.*"

  environment_variables = [
    {
      "name"  = "FORCE_RELEASE"
      "value" = "false"
    },
    {
      "name"  = "GRADLE_USER_HOME"
      "value" = local.gradle_user_home
    },
    {
      "name"  = "ECI"
      "value" = local.product_domain
    },
  ]
}
