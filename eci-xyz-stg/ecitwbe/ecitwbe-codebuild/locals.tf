locals {
  build_type            = "java-build"
  image                 = "aws/codebuild/standard:4.0"
  product_domain        = "eci"
  repository_name       = "ent-enterprise-common"
  codebuild_name        = "ecitwbe"
  gradle_user_home      = ".gradle"
  codebuild_role_arn    = "arn:aws:iam::{account_id}:role/service-role/codebuild.amazonaws.com/{service_role_for_eci_codebuild}"
  environment           = "staging"

  additional_tags = {
    Team = "ecitbwp"
  }
}
