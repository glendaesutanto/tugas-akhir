import errno
import json
import logging
import os
import sys
import tempfile
import traceback
import zipfile

import boto3
import botocore
from boto3.session import Session

logger = logging.getLogger()
logger.setLevel(logging.INFO)

deployment_jump_roles_map = json.loads(os.environ['DEPLOYMENT_TARGET_ACCOUNT_JSON'])
codepipeline_client = boto3.client("codepipeline")


def handle_lambda_exception(message):
    exception_type, exception_value, exception_traceback = sys.exc_info()
    traceback_string = traceback.format_exception(
        exception_type, exception_value, exception_traceback)
    err_msg = json.dumps({
        "userMessage": message,
        "exceptionType": exception_type.__name__,
        "exceptionMessage": str(exception_value),
        "stackTrace": traceback_string
    })
    logger.error(err_msg)
    raise RuntimeError(err_msg)


def report_failure(job_id, error):
    codepipeline_client.put_job_failure_result(jobId=job_id, failureDetails={
        "type": "JobFailed",
        "message": str(error)
    })
    logger.error("Aborting codepipeline job due to : %s", error)
    return


def report_success(job_id):
    codepipeline_client.put_job_success_result(jobId=job_id)


def get_artifact(s3_client, artifact):
    """Get manifest from artifact bucket
    Downloads the build manifest file from the S3 artifact then returns the content as dict
    Args:
        s3_client : An S3 client with the appropriate credentials from codepipeline event
        artifact : The artifact to download
    Returns:
        dict
    """
    try:
        manifest_file = s3_client.get_object(
            Bucket=artifact["location"]["s3Location"]["bucketName"],
            Key=artifact["location"]["s3Location"]["objectKey"]
        )
        return json.loads(manifest_file['Body'].read())
    except Exception:
        raise FileNotFoundError(errno.ENOENT,
                                f"Failed while downloading manifest from {artifact['location']['s3Location']['bucketName']}",
                                artifact["location"]["s3Location"]["objectKey"])


def __zip_and_upload_files_as_artifact(s3_client, file_dict, target_s3_location, kms_key_id):
    """Upload temporary files as artifact
    Args:
        s3_client : An S3 client with the appropriate credentials from codepipeline event
        file_dict : list of temporary file name and its location
        target_s3_location : The OutputArtifact to upload from codepipeline event
        kms_key_id : KMS - Customer managed key to encrypt codepipeline artifact
    Returns:
        None
    """
    output_artifact_file = os.path.join(tempfile.gettempdir(), "artifact.zip")
    zf = zipfile.ZipFile(output_artifact_file, mode='w',
                         compression=zipfile.ZIP_DEFLATED)
    try:
        for file_path, arc_name in file_dict.items():
            zf.write(file_path, arc_name)
    finally:
        zf.close()
    bucket = target_s3_location['bucketName']
    key = target_s3_location['objectKey']
    extra_option = {
        "ServerSideEncryption": "aws:kms",
        "SSEKMSKeyId": kms_key_id
    }
    s3_client.upload_file(output_artifact_file, bucket,
                          key, ExtraArgs=extra_option)
    logger.info("Uploaded to : %s/%s", bucket, key)


def create_taskdef_template(ecs_client, ecs_cluster_name, ecs_service_name, container_name, image_info):
    """Create task definition template
    Create a task definition JSON file as template for CodeDeployToECS action to create new task definition resource
    Args:
        ecs_client : An ECS client with the appropriate credentials from assumed role credential to deployer ARN
        ecs_cluster_name : ECS cluster name
        ecs_service_name : ECS service name
        container_name : main (backend service) container name (e.g : service_name-app)
        image_info : ECR Image information from build_manifest file
    Returns:
        dict : task definition template
    """
    ecs_service = ecs_client.describe_services(cluster=ecs_cluster_name, services=[
        ecs_service_name])["services"][0]

    # current active task definition
    current_taskdef = ecs_client.describe_task_definition(
        taskDefinition=ecs_service["taskDefinition"])["taskDefinition"]

    # latest task definition revision
    latest_taskdef = ecs_client.describe_task_definition(
        taskDefinition=current_taskdef["family"])["taskDefinition"]

    td_family, td_container_definitions = latest_taskdef[
        "family"], latest_taskdef["containerDefinitions"]
    service_container = next(
        container for container in td_container_definitions if container["name"] == container_name)
    service_container["logConfiguration"]["options"]["awslogs-stream-prefix"] = image_info['tags'][0]
    # Placeholder for CodeDeployToECS action
    service_container["image"] = "<IMAGE_NAME>"

    task_def_template = {
        "family": td_family,
        "containerDefinitions": td_container_definitions,
        "executionRoleArn": latest_taskdef['executionRoleArn'],
        "requiresCompatibilities": latest_taskdef['requiresCompatibilities'],
        "cpu": latest_taskdef['cpu'],
        "memory": latest_taskdef['memory'],
        "tags": [
            dict(key="Name", value=td_family),
            dict(key="ManagedBy", value="ecs_deployment_pipeline"),
            dict(key="Version", value=image_info['tags'][0]),
        ]
    }
    if 'taskRoleArn' in latest_taskdef:
        task_def_template["taskRoleArn"] = latest_taskdef['taskRoleArn']
    if 'volumes' in latest_taskdef:
        task_def_template["volumes"] = latest_taskdef['volumes']
    if 'placementConstraints' in latest_taskdef:
        task_def_template["placementConstraints"] = latest_taskdef['placementConstraints']
    if 'networkMode' in latest_taskdef:
        task_def_template["networkMode"] = latest_taskdef['networkMode']
    return task_def_template


def create_and_upload_output_artifact(appspec_dict, taskdef_dict, image_uri, s3_client, appspec_s3_target, kms_key_id):
    artifact_files = {}
    appspec_file_path = os.path.join(tempfile.gettempdir(), "appspec.json")
    taskdef_file_path = os.path.join(tempfile.gettempdir(), "taskdef.json")
    image_detail_file_path = os.path.join(
        tempfile.gettempdir(), "imageDetail.json")

    artifact_files[appspec_file_path] = "appspec.json"
    artifact_files[taskdef_file_path] = "taskdef.json"
    artifact_files[image_detail_file_path] = "imageDetail.json"

    with open(appspec_file_path, "w") as appspec_file:
        json.dump(appspec_dict, appspec_file)
    with open(taskdef_file_path, "w") as taskdef_file:
        json.dump(taskdef_dict, taskdef_file)
    with open(image_detail_file_path, "w") as image_detail_file:
        json.dump({"ImageURI": image_uri}, image_detail_file)

    __zip_and_upload_files_as_artifact(
        s3_client, artifact_files, appspec_s3_target, kms_key_id)


def setup_cross_account_client(role_arn, aws_service_name, pipeline_job_id):
    session_name = f"predeployment_lambda-{pipeline_job_id}"
    assumed_role = boto3.client("sts").assume_role(
        RoleArn=role_arn, RoleSessionName=session_name).get("Credentials")

    key_id = assumed_role['AccessKeyId']
    key_secret = assumed_role['SecretAccessKey']
    session_token = assumed_role['SessionToken']

    session = Session(aws_access_key_id=key_id,
                      aws_secret_access_key=key_secret,
                      aws_session_token=session_token)
    logger.info("Setting up %s client for : %s (%s)",
                aws_service_name, role_arn, session_name)
    return session.client(aws_service_name, region_name=os.environ["AWS_REGION"])


def setup_s3_client(job_data):
    """Creates an S3 client
    Uses the credentials passed in the event by CodePipeline. These credentials can be used to access the artifact bucket.
    Args:
        job_data: The job data structure
    Returns:
        An S3 client with the appropriate credentials
    """
    key_id = job_data['artifactCredentials']['accessKeyId']
    key_secret = job_data['artifactCredentials']['secretAccessKey']
    session_token = job_data['artifactCredentials']['sessionToken']

    session = Session(aws_access_key_id=key_id,
                      aws_secret_access_key=key_secret,
                      aws_session_token=session_token)
    return session.client('s3', config=botocore.client.Config(signature_version='s3v4'))


def get_ecs_cluster_and_service_name(codedeploy_aplication_name, codedeploy_deployment_group_name, deployment_jump_role_arn, job_id):
    codedeploy_client = setup_cross_account_client(
        role_arn=deployment_jump_role_arn, aws_service_name="codedeploy", pipeline_job_id=job_id)

    try:
        deployment_group_info = codedeploy_client.get_deployment_group(
            applicationName=codedeploy_aplication_name, deploymentGroupName=codedeploy_deployment_group_name)["deploymentGroupInfo"]
    except codedeploy_client.exceptions.DeploymentGroupDoesNotExistException as e:
        handle_lambda_exception(
            f"CodeDeploy not found. Check whether the codedeploy is in the same account as {deployment_jump_role_arn}")
        raise RuntimeError(e)
    logger.debug(deployment_group_info)
    if "ecsServices" not in deployment_group_info:
        return report_failure(job_id, "The codedeploy doesn't deploy to ECS")
    return deployment_group_info["ecsServices"][0]["clusterName"], deployment_group_info["ecsServices"][0]["serviceName"]


# The function is only compatible for codepipeline with exactly 1 input and 1 output artifact
def handler(event, context):
    codepipeline_job = event["CodePipeline.job"]
    job_id = codepipeline_job["id"]
    job_data = codepipeline_job["data"]
    if "UserParameters" not in job_data["actionConfiguration"]["configuration"]:
        return report_failure(
            job_id, "Lambda user parameters are not configured in code pipeline, doing no-op")
    try:
        user_parameters = json.loads(
            job_data["actionConfiguration"]["configuration"]["UserParameters"])
        kms_key_id = job_data['encryptionKey']['id']
        dst_role_arn = f"arn:aws:iam::{deployment_jump_roles_map[user_parameters['environment']]}:role/crossaccount/be_dss-ecs_predeployment_jump_role"
        s3_client = setup_s3_client(job_data=job_data)
        if "codedeploy_deployment_group" not in user_parameters or "codedeploy_application" not in user_parameters:
            return report_failure(
                job_id, "No 'codedeploy_application' or 'codedeploy_deployment_group' in the lambda user parameters")
        ecs_cluster_name, ecs_service_name = get_ecs_cluster_and_service_name(
            user_parameters["codedeploy_application"], user_parameters["codedeploy_deployment_group"], dst_role_arn, job_id)

        ecs_client = setup_cross_account_client(
            role_arn=dst_role_arn, aws_service_name="ecs", pipeline_job_id=job_id)

        # for now, as there's just 1 input and 1 output artifact, let's do this for more flexibility in artifact naming
        build_manifest_artifact = job_data["inputArtifacts"][0]
        appspec_s3_target = job_data["outputArtifacts"][0]["location"]["s3Location"]
        # build_manifest_artifact = next(item for item in job_data["inputArtifacts"] if item["name"] == "buildManifest")
        # appspec_s3_target = next(item for item in job_data["outputArtifacts"] if item["name"] == f"{user_parameters['environment']}Appspec")["location"]["s3Location"]

        bm = get_artifact(s3_client, build_manifest_artifact)

        if bm:
            appspec_dict = bm["deployment"][f"{user_parameters['environment']}Appspec"]
            container_name = appspec_dict["Resources"][0]["TargetService"][
                "Properties"]["LoadBalancerInfo"]["ContainerName"]
            image_uri = f"{bm['deployment']['containerInfo']['name']}:{bm['deployment']['containerInfo']['tags'][0]}"

            # Make sure to use "<TASK_DEFINITION>" placeholder for appspec file, required by CodeDeployToECS action
            appspec_dict["Resources"][0]["TargetService"]["Properties"]["TaskDefinition"] = "<TASK_DEFINITION>"
            new_taskdef_json = create_taskdef_template(ecs_client=ecs_client,
                                                       ecs_cluster_name=ecs_cluster_name,
                                                       ecs_service_name=ecs_service_name,
                                                       container_name=container_name,
                                                       image_info=bm["deployment"]["containerInfo"])
            logger.info(json.dumps(new_taskdef_json))
            create_and_upload_output_artifact(appspec_dict=appspec_dict, taskdef_dict=new_taskdef_json,
                                              image_uri=image_uri, kms_key_id=kms_key_id, s3_client=s3_client,
                                              appspec_s3_target=appspec_s3_target)
            report_success(job_id)
        else:
            return report_failure(job_id, "The build manifest doesn't exist")
    except Exception as e:
        handle_lambda_exception(f"Unable to process the request {job_id}: {e}")
        return report_failure(job_id, "Unexpected error. See the lambda execution log.")

