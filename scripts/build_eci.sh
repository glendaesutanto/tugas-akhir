#!/bin/bash
set -evo pipefail
CURRENT_DIR="$(dirname "$0")"
${CURRENT_DIR}/init_git.sh
if [ -n "${CI_NAME}" ]; then
     ${CURRENT_DIR}/init_${CI_NAME}.sh
     ./gradlew :tbi-workflow-platform-cmpt:build
else
    # local build
    ./gradlew :tbi-workflow-platform-cmpt:build --write-locks
fi

if [[ ${DETECTED_VERSION} =~ ${RELEASE_TAG_PATTERN_ECI} ]]; then
  IFS='-' read -r -a APP_NAME <<< "$DETECTED_VERSION"
  APP_NAME=ecitwbe
  for i in $(ls -d */ | sed 's/\///g'); do
    DIR=${i%%//};
    echo "Dir_name: $DIR"
    CMPT_DIR=$(echo $DIR|rev|cut -d'-' -f 1|rev);
    if [[ "$CMPT_DIR" = "cmpt" ]] || [[ "$CMPT_DIR" = "service" ]]; then
      if [ ! -z "$APP_NAME" ]; then
        if [[ "$DIR" = "tbi-workflow-platform-cmpt" ]]; then
          ./scripts/assume_role.sh -r arn:aws:iam::{account_id}:role/{role_name}
          # login to tvlk-build ECR (target registry)
          aws ecr get-login-password --region ap-southeast-1 --profile beiartf | docker login --username AWS --password-stdin {account_id}.dkr.ecr.ap-southeast-1.amazonaws.com
          # and tvlk-tsi-prod ECR (base image registry)
          aws ecr get-login-password --region ap-southeast-1 --profile beiartf | docker login --username AWS --password-stdin {account_id}.dkr.ecr.ap-southeast-1.amazonaws.com

          IMAGE_VERSION=$(git rev-parse --short HEAD)-${APP_NAME}

          # release 1-service
          ./gradlew :tbi-workflow-platform-cmpt:dockerPublishRemote :tbi-workflow-platform-cmpt:uploadBuildManifest -Pversion=${IMAGE_VERSION} -Daws.profile=default
        fi
      fi
    fi;
  done;
fi;
