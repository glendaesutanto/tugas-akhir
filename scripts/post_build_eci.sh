#!/bin/bash
set -evo pipefail
CURRENT_DIR="$(dirname "$0")"
. ${CURRENT_DIR}/init_git.sh
. ${CURRENT_DIR}/init_${CI_NAME}.sh

BUILD_STATUS="succeed"
if [[ ${BUILD_RETURN_VALUE} == 0 ]]; then
    if [[ ${DETECTED_VERSION} =~ ${VERSION_PATTERN} ]]; then
        BUILD_MESSAGE="libraries version ${DETECTED_VERSION} published"
    fi
    if [[ ${DETECTED_VERSION} =~ ${RELEASE_TAG_PATTERN} ]]; then
        APP_NAME=$(echo ${DETECTED_VERSION}| cut -d'-' -f 2)
        BUILD_MESSAGE="service ${APP_NAME} version $(git rev-parse --short HEAD) uploaded"
    fi
else
    BUILD_STATUS="failed"
    BUILD_MESSAGE="see build log for more details"
fi

echo "$BUILD_STATUS" "$BUILD_MESSAGE"
