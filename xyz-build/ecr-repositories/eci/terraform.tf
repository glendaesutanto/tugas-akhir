terraform {
  backend "s3" {
    bucket         = "default-terraform-state-ap-southeast-1-{account_id}"
    key            = "path/to/ecr-repositories/tfstate"
    region         = "ap-southeast-1"
    dynamodb_table = "default-terraform-state-ap-southeast-1-{account_id}"
  }
}
