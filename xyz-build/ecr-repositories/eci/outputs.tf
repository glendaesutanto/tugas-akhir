output "ecitwbe_repository_url" {
  value       = "${module.ecitwbe.repository_url}"
  description = "The URL of ECITBWP app (ECITWBE) repository"
}
