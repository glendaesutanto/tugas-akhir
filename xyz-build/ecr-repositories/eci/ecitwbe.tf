module "ecitwbe" {
  source         = "github.com/traveloka/terraform-aws-ecr-repository/tree/v0.1.1/modules/with_default_policies"
  product_domain = "${local.product_domain}"
  environment    = "${local.environment}"
  aws_org_id     = "${local.tvlk_org_id}"
  service_name   = "ecitwbe"
}
